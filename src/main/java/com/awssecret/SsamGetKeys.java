package com.awssecret;

import java.util.HashMap;
import java.util.List;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.secretsmanager.AWSSecretsManager;
import com.amazonaws.services.secretsmanager.AWSSecretsManagerClientBuilder;
import com.amazonaws.services.secretsmanager.model.GetSecretValueRequest;
import com.amazonaws.services.secretsmanager.model.GetSecretValueResult;
import com.amazonaws.services.simplesystemsmanagement.AWSSimpleSystemsManagement;
import com.amazonaws.services.simplesystemsmanagement.AWSSimpleSystemsManagementClientBuilder;
import com.amazonaws.services.simplesystemsmanagement.model.GetParameterResult;
import com.amazonaws.services.simplesystemsmanagement.model.GetParametersByPathRequest;
import com.amazonaws.services.simplesystemsmanagement.model.GetParametersByPathResult;
import com.amazonaws.services.simplesystemsmanagement.model.Parameter;

public class SsamGetKeys {
	
	
	
	
	public static String GetSecretsManagerValues(String accessKey, String secretKey, String region, String location) {
		BasicAWSCredentials awsCredas = new BasicAWSCredentials(accessKey, secretKey);
		AWSSecretsManager client = AWSSecretsManagerClientBuilder.standard().withRegion(region).withCredentials(new AWSStaticCredentialsProvider(awsCredas)).build();
		
		GetSecretValueRequest req = new GetSecretValueRequest().withSecretId(location);
		GetSecretValueResult getSecretValueResult = null;
		
		getSecretValueResult = client.getSecretValue(req);
		return getSecretValueResult.getSecretString();
		
		

	}
	
	public static Object GetSsamValues(String accessKey, String secretKey, String region, String location) {
		BasicAWSCredentials awsCreds = new BasicAWSCredentials(accessKey, secretKey);
		AWSSimpleSystemsManagement client = AWSSimpleSystemsManagementClientBuilder.standard().withRegion(region).withCredentials(new AWSStaticCredentialsProvider(awsCreds)).build();
		
		
		HashMap<String, String > hmap = new HashMap<>();
		GetParametersByPathRequest paramRequest = new GetParametersByPathRequest().withPath(location);
		
		GetParametersByPathResult res = client.getParametersByPath(paramRequest);
		
		for (Parameter list : res.getParameters()) {
			hmap.put(list.getName(),list.getValue());
		}
		return hmap;
		

	}

}

